<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:lib="http://www.zvon.org/library">
<xsl:template match="/">
  <html>
  <body>
    <table border="1">
      <tr>
        <th>Title</th>
        <th>Pages</th>
      </tr>
	  <xsl:for-each select="rdf:RDF/rdf:Description">
      <xsl:sort select="lib:pages"/>
      <tr>
          <xsl:choose>
          <xsl:when test="lib:pages &gt;0">
      <td>
          <xsl:value-of select="@about"/>
      </td>
      <td>
          <xsl:number value="lib:pages"/>
      </td>
          </xsl:when>
          </xsl:choose>
      </tr>
          </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>

