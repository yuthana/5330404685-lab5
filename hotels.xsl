<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="//kml:Document">
	<html>
		<body>
			<img src="{kml:Style/kml:IconStyle/kml:Icon/kml:href}"/>
			<b>
			<xsl:value-of select="kml:name"/>
			</b>
			
			<tr></tr>
			
			<tr>
			<xsl:text>List of hotel</xsl:text>
			</tr>
			
			<ul>
                <xsl:for-each select="kml:Placemark">
                        <xsl:sort select="kml:name"/>
                        <li>
                            <xsl:value-of select="kml:name"/>
                            <ul>
                                <li>
                                    <a href="{substring-after(substring-before(kml:description,'&#34;&gt;&lt;'),'&lt;a href=&#34;')}">
                                        <xsl:value-of select="substring-after(substring-before(kml:description,'&#34;&gt;&lt;'),'&lt;a href=&#34;')"/>
                                    </a>
                                </li>
                                <li>
                                    Coordinates:
                                    <xsl:value-of select="kml:Point/kml:coordinates"/>
                                </li>
                            </ul>
                        </li>
                </xsl:for-each>
            </ul>
			
			
		</body>
	</html>
</xsl:template>
</xsl:stylesheet>