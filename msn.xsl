<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<html>
		<body>
				<Text Style="font-family:Segoe UI; color:#FF0000; ">
					<xsl:text>[Conversation started on </xsl:text><xsl:value-of select="Log/Message/@Date"/><xsl:text> </xsl:text>
					<xsl:value-of select="Log/Message/@Time"/><xsl:text>]</xsl:text>
				</Text>
			
			<tr>	
				<Text Style="font-family:Segoe UI; ">
					<xsl:text>[</xsl:text><xsl:value-of select="Log/Message/@Date"/><xsl:text> </xsl:text><xsl:value-of select="Log/Message/@Time"/><xsl:text>]</xsl:text>
				</Text>
				<Text Style="font-family:Segoe UI; color:orange; ">
					<xsl:value-of select="Log/Message/From/User/@FriendlyName"/>
				</Text>
					<xsl:text>:</xsl:text>
				<Text Style="font-family:Segoe UI; color:orange; ">
					<xsl:value-of select="Log/Message/Text"/>
				</Text>
			</tr>		
			
			<tr>	
				<Text Style="font-family:Segoe UI; ">
					<xsl:text>[</xsl:text><xsl:value-of select="Log/Message[2]/@Date"/><xsl:text> </xsl:text><xsl:value-of select="Log/Message[2]/@Time"/><xsl:text>]</xsl:text>
				</Text>
				<Text Style="font-family:Segoe UI; color:green; ">
					<xsl:value-of select="Log/Message[2]/From/User/@FriendlyName"/>
				</Text>
					<xsl:text>:</xsl:text>
				<Text Style="font-family:Segoe UI; color:green; ">
					<xsl:value-of select="Log/Message[2]/Text"/>
				</Text>
			</tr>	
			
			<tr>	
				<Text Style="font-family:Segoe UI; ">
					<xsl:text>[</xsl:text><xsl:value-of select="Log/Message[3]/@Date"/><xsl:text> </xsl:text><xsl:value-of select="Log/Message[3]/@Time"/><xsl:text>]</xsl:text>
				</Text>
				<Text Style="font-family:Segoe UI; color:orange; ">
					<xsl:value-of select="Log/Message[3]/From/User/@FriendlyName"/>
				</Text>
					<xsl:text>:</xsl:text>
				<Text Style="font-family:Segoe UI; color:orange; ">
					<xsl:value-of select="Log/Message[3]/Text"/>
				</Text>
			</tr>	
			
			<tr>	
				<Text Style="font-family:Segoe UI; ">
					<xsl:text>[</xsl:text><xsl:value-of select="Log/Message[4]/@Date"/><xsl:text> </xsl:text><xsl:value-of select="Log/Message[4]/@Time"/><xsl:text>]</xsl:text>
				</Text>
				<Text Style="font-family:Segoe UI; color:green; ">
					<xsl:value-of select="Log/Message[4]/From/User/@FriendlyName"/>
				</Text>
					<xsl:text>:</xsl:text>
				<Text Style="font-family:Segoe UI; color:green; ">
					<xsl:value-of select="Log/Message[4]/Text"/>
				</Text>
			</tr>	
			
		</body>
	</html>
</xsl:template>
</xsl:stylesheet>